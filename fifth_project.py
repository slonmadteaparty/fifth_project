import pygame
import random

pygame.sprite.Sprite
pygame.init()
water = "#0000aa"
grass = "#00aa00"
mountain = "#555555"
forest = "#ffff55"
dirt = "#880000"
screenw = 1250
screenh = 1080
blockw = 40
blockh = 40
perwater = 15
permountain = 35
perforest = 50
numofblockw = screenw/blockw
numofblockh = screenh/blockh

class Scout(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.x = x
        self.y = y
        self.image = pygame.Surface((30,30))
        self.image. fill(pygame.Color('#ff0000'))
        self.rect = pygame.Rect(x,y,40,40)
        self.select = False
    def click(self, key, pos):
        if key == 1:
            if self.point_in_object(pos) == True:
                self.select = True
            else:
                self.select = False
        
        elif key == 3:
            if self.select:
                self.warp(pos)
                #
                 #   scout.fill(pygame.Color(water))
    def warp(self, pos):
        self.x = pos[0] / blockw * blockw
        self.y = pos[1] / blockh * blockh
        self.rect.x = pos[0] / blockw * blockw
        self.rect.y = pos[1] / blockh * blockh 
    def update(self, delta_x, delta_y):
        self.x += delta_x
        self.y += delta_y
        self.rect.x += delta_x
        self.rect.y += delta_y
    def point_in_object(self, pos):
        if self.x <= pos[0] <= self.x + 35 and self.y <= pos[1] <= self.y + 35:
            return True
        else:
            return False
    def draw(self, screen):
        screen.blit(self.image, (self.rect.x + 5, self.rect.y + 5))
        


rand = 0
map_=([])
screen=pygame.display.set_mode((screenw,screenh))
pygame.display.set_caption("civ")

for i in xrange(numofblockw):
    map_.append([])
    for j in xrange(numofblockh):
        rand=random.randint(1,100)
        surface=pygame.Surface((blockw,blockh))
        color=grass
        if 0<rand<perwater :
            color=water
        if perwater<rand<permountain :
            color=mountain
        if permountain<rand<perforest :
            color=forest

        surface.fill(pygame.Color(color))
        map_[i].append(surface)


bg=pygame.Surface((screenw,screenh))
bg.fill(pygame.Color(dirt))

scout = Scout(120, 120)



in_loop=True
while in_loop:
    for i in xrange(numofblockw):
        for j in xrange(numofblockh):
            screen.blit(map_[i][j],(i*blockw,j*blockh))
    delta_x = 0
    delta_y = 0
    for event in pygame.event.get():
        if event.type==pygame.QUIT:
            pygame.display.quit()
            in_loop=False
        if event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:
            delta_x = 40
        if event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT:
            delta_x = -40
        if event.type == pygame.KEYDOWN and event.key == pygame.K_DOWN:
            delta_y = 40
        if event.type == pygame.KEYDOWN and event.key == pygame.K_UP:
            delta_y = -40
        elif event.type == pygame.MOUSEBUTTONDOWN:
            scout.click(event.button , event.pos)  
    
    scout.update(delta_x, delta_y)
    scout.draw(screen)
    pygame.display.update()
